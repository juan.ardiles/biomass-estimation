# Biomass Estimation
https://colab.research.google.com/drive/1f6BbHpw7PO7IbMBiLKDpm8kjXx1BprWe?usp=sharing
## Integrantes G5

Juan Ardiles 201721019-2

Ulises Leiva 201521022-5

Camila Herrera 201730030-2

Axel Pérez 201730035-3

Juan Vilches 201930040-7

## Manual de Instalación y de Usuario

### Mask-RCNN para deteccion de aletas

![alt text](img/MaskRCNN.png?raw=true)

Se implementa Mask-RCNN para deteccion de aletas internas, externas y cola. Para Windows, se debe tener Visual C++ 2015 en el path. Para lograr implementarlo se requiere Pyhton==3.7.9, cuda=10.0.0 y:

```
pip3 install -r requirements.txt
python3 setup.py install
pip3 install git+https://github.com/philferriere/cocoapi
```
Despues se debe descargar https://drive.google.com/drive/folders/1l41AQlSg7NjhTzkSqk4UvY0rU9lxlFzX, y se debe cambiar los path del Mask-RCNN.ipynb segun correspondan en su computador, tambien hay que hacer cambios IMPORTANTE en el model.py, cambiar el self.log_dir = "\\Users\\jmard\\Jupyter_Archivos\\PDI\\Mask-RCNN-TF2-master\\logs\\" de linea 2279 y 2345 a su direccion correspondiente, ya que no funciona correctamente los path automaticos para windows.

Como ya en este repositorio esta mask_rcnn_fins_0031.h5, se puede saltar la parte de training y utilizar directamente la deteccion en  la seccion Deteccion de imagenes no etiquetadas (PRIMERO CORRER TODO ANTES DE TRAINING).


### Operaciones Morfologicas para corregir la segmentacion

![alt text](img/opening.png?raw=true)


Para este se instalo lo siguiente, de los ya instalado previmente.
```
pip3 install cv2
pip3 install matplotlib
```
Igualmente esto esta implementado en el  [Colab](https://colab.research.google.com/drive/1f6BbHpw7PO7IbMBiLKDpm8kjXx1BprWe?usp=sharing)
### Estimacion de biomasa
Para la estimacion de Biomasa implementado en el [Colab](https://colab.research.google.com/drive/1f6BbHpw7PO7IbMBiLKDpm8kjXx1BprWe?usp=sharing), se utilizaron 2 metodos diferentes:

-Convex-hull con Riemman

-Semi-Elipse

Para probar estos metodos se pueden agregar carpetas al Colab que sigan el siguiente formato:

-Mapa de profundidad: "biomass-estimation/ImageN/depth.npy"

-Nube de puntos:      "biomass-estimation/ImageN/point_cloud.npy"

-Imagen normal:       "biomass-estimation/ImageN/image.png"

-Mascara de aletas:   "biomass-estimation/ImageN/custom_mask.png"

-Mascara del pez:     "biomass-estimation/ImageN/mask.png"

-Masa:                "biomass-estimation/ImageN/masa.txt"
 
 Donde "ImageN" corresponde al numero de carpeta que debe empezar desde 6, ya que ya existen 5 carpetas integradas para probar el codigo.
